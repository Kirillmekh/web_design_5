var points = new Array(),
colorInput = document.getElementById("color");

function start(e)
{
  var mouseX = e.pageX - canvas.offsetLeft - 200;
  var mouseY = e.pageY - canvas.offsetTop - 144;
  paint = true;
  ctx.beginPath();
  ctx.moveTo(mouseX, mouseY);
  points[points.length] = [mouseX, mouseY];
};

function draw(e)
{
  if (paint)
  {
    ctx.colorInput = colorInput.value;
    var mouseX = e.pageX - canvas.offsetLeft - 200;
    var mouseY = e.pageY - canvas.offsetTop - 144;
    ctx.lineTo(mouseX, mouseY);
    ctx.stroke();
    ctx.strokeStyle = colorInput.value;
    ctx.lineJoin = ctx.lineCap = 'round';
    points[points.length] = [mouseX, mouseY];
  }
}

function stop(e)
{
  paint = false;
  var s = JSON.stringify(points);
  localStorage['lines'] = s;
}

function clearCanvas()
{
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears
}

function onChangeTrackBar()
{
  let trackbar = document.getElementById('trackbar');
  console.log(trackbar.value);
  ctx.lineWidth = trackbar.value;
  document.getElementById("trackBarValue").innerHTML = trackbar.value;
}

var paint = false;
var canvas = document.getElementById('draw');
var ctx = canvas.getContext("2d");
canvas.addEventListener('mousedown', start);
canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mouseup', stop);
